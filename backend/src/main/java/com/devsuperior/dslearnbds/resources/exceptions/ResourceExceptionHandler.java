package com.devsuperior.dslearnbds.resources.exceptions;

import com.devsuperior.dslearnbds.services.exceptions.DataBaseException;
import com.devsuperior.dslearnbds.services.exceptions.ForbiddenException;
import com.devsuperior.dslearnbds.services.exceptions.UnauthorizedException;
import org.springframework.boot.context.config.ConfigDataResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;

@ControllerAdvice
public class ResourceExceptionHandler {

    /* Classe responsável por interceptar alguma exceção que ocorra na camada Resource */

    @ExceptionHandler(ConfigDataResourceNotFoundException.class) //Captura a exceção do tipo EntityNotFoundException
    public ResponseEntity<StandardError> entityNotFound(ConfigDataResourceNotFoundException e, HttpServletRequest request){
        StandardError err = new StandardError();
        err.setTimestamp(Instant.now());
        err.setStatus(HttpStatus.NOT_FOUND.value());
        err.setError("Resource not Found");
        err.setMessage(e.getMessage());       //Mensagem informada no Service
        err.setPath(request.getRequestURI()); //Pega o caminho onde foi feito a requisição Ex.:"/categories/5"

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(err);
    }

    @ExceptionHandler(DataBaseException.class)
    public ResponseEntity<StandardError> database(DataBaseException e, HttpServletRequest request){
        StandardError err = new StandardError();
        err.setTimestamp(Instant.now());
        err.setStatus(HttpStatus.BAD_REQUEST.value());
        err.setError("Database exception");
        err.setMessage(e.getMessage());       //Mensagem informada no Service
        err.setPath(request.getRequestURI()); //Pega o caminho onde foi feito a requisição Ex.:"/categories/5"

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ValidationError> validation(MethodArgumentNotValidException e, HttpServletRequest request){
        ValidationError err = new ValidationError();
        err.setTimestamp(Instant.now());
        err.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
        err.setError("Validation exception");
        err.setMessage(e.getMessage());       //Mensagem informada no Service
        err.setPath(request.getRequestURI()); //Pega o caminho onde foi feito a requisição Ex.:"/categories/5"

        //Percorre a lista de Erros, gerados apartir da validação dos Fields e adiciona na lista de erros da classe ValidationError
        for (FieldError f: e.getBindingResult().getFieldErrors()){
            err.addError(f.getField(), f.getDefaultMessage());
        }

        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(err);
    }

    @ExceptionHandler(ForbiddenException.class)
    public ResponseEntity<OAuthCustomError> forbidden(ForbiddenException e, HttpServletRequest request){
        OAuthCustomError err = new OAuthCustomError("Forbidden", e.getMessage());

        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(err);
    }

    @ExceptionHandler(UnauthorizedException.class)
    public ResponseEntity<OAuthCustomError> unauthorizedException(UnauthorizedException e, HttpServletRequest request){
        OAuthCustomError err = new OAuthCustomError("UnauthorizedException", e.getMessage());

        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(err);
    }
}
